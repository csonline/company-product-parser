const {URL} = require('url')
const _ = require('lodash')

let parser = (url) => {
  let urlData = new URL(url)
  let urlHostname = _.reverse(urlData.hostname.split('.')).join('_')
  let pathname = urlData.pathname.replace(/\//g, '-')
  pathname = _.trim(pathname, '-')
  return pathname ? [urlHostname, pathname].join('|') : urlHostname
}

module.exports = {
  parser
}
