const {parser} = require('./index.js')

let test = 'https://stackoverflow.com/questions/13272406/javascript-string-to-array-conversion'
let test2 = 'https://lodash.com/docs/4.17.4#replace'
let test3 = 'https://cloud.google.com/nodejs/getting-started/hello-world'
let test4 = 'https://www.test-1.banmedica.cl/'
let test5 = 'https://www.test-1.banmedica.cl/planes/plan-de-salud'

console.log(parser(test))
console.log(parser(test2))
console.log(parser(test3))
console.log(parser(test4))
console.log(parser(test5))
